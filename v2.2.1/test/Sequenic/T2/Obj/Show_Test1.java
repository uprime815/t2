package Sequenic.T2.Obj;
import java.util.* ;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This test will always pass. Look at the output manually...
 */
public class Show_Test1 {

    static public class COBA0 {
        public Integer z = 0 ;
    }

    
    static public class COBA extends COBA0 {
        public int x = 10 ;
        public COBA next = null ;
    }
    
    static Show shower = new Show(4,4) ;
  
    /**
     * Test of showWithContNum method, of class Show.
     */
    @Test
    public void testContinousNumbering() {
        System.out.println("@@@ Test continuous numbering...");
        shower.resetContinousNumbering() ;
        COBA u = new COBA() ;
        System.out.println("u = " + shower.showWithContNum(u)); 
        int id_u_0 = shower.pool.get(u) ;
        assert shower.pool.containsKey(u) ;
        COBA v = new COBA() ;
        u.next = v ;
        assert shower.pool.containsKey(u) ;
        System.out.println("u = " + shower.showWithContNum(u)); 
        int id_u_1 = shower.pool.get(u) ;
        int id_v_1 = shower.pool.get(v) ;
        assert id_u_0 == id_u_1 ;
        assert id_u_0 != id_v_1 ;
        System.out.println("v = " + shower.showWithContNum(v));
        int id_u_2 = shower.pool.get(u) ;
        int id_v_2 = shower.pool.get(v) ;
        assert id_u_0 == id_u_1 ;
        assert id_u_0 == id_u_2 ;
        assert id_u_0 != id_v_1 ;
        assert id_v_1 == id_v_2 ;
    }
     
    @Test
    public void testVeryCompactShow() {
        System.out.println("test very compact show ...");
        String s = shower.showWithContNum(System.out) ;
        System.out.println("System.out = " + s);
        assert ! s.contains("@") ;
    }

    @Test
    public void testFilteredShow() {        
        System.out.println("test filtered show ...");
        
        LinkedList<String> fieldsToShow = new LinkedList<String> () ;
        fieldsToShow.add("next") ;
        fieldsToShow.add("z") ;
        Show.showFilter.put(COBA.class, fieldsToShow) ;        
        COBA u = new COBA() ;
        COBA v = new COBA() ;
        u.next = v ;
        shower.resetContinousNumbering() ;
        System.out.println("u = " + shower.showWithContNum(u));
        System.out.println("v = " + shower.showWithContNum(v));
        assert ! (shower.showWithContNum(u)).contains(" x ")  ;
        assert   (shower.showWithContNum(u)).contains("next ")  ;                
        assert ! (shower.showWithContNum(v)).contains(" x ") ;
        assert   (shower.showWithContNum(v)).contains("next ")  ;                
    }
 
}