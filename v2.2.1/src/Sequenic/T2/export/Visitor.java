package Sequenic.T2.export;

import Sequenic.T2.Seq.CALL_METHOD;
import Sequenic.T2.Seq.CONST;
import Sequenic.T2.Seq.CREATE_OBJECT;
import Sequenic.T2.Seq.MkValStep;
import Sequenic.T2.Seq.REF;
import Sequenic.T2.Seq.TraceStep;

/**
 * Extend and implement this class to do something useful at each node.
 * This roughly follows the Visitor pattern.
 * 
 * @author Christiaan Hees
 */
public abstract class Visitor {
	public abstract void visit(MkValStep mvs);
	public abstract void visit(CREATE_OBJECT co);
	public abstract void visit(REF ref);
	public abstract void visit(CONST c);
	public abstract void visit(TraceStep ts);
	public abstract void visit(CALL_METHOD cm);
}
