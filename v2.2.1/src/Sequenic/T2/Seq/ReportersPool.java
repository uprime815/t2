/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;
import java.io.* ;
import java.util.*;

/**
 * A ReportersPool provides a pool of various reporters.
 * It provides the same interface as a reporter, but it will call
 * all the reporters in its pool. In this way we can produce multiple reports
 * (e.g. in both text and HTML). Currently we only have simple text format
 * though :)
 *
 * <p> A test engine should call ReportersPool, and not a reporter directly.
 *
 */
public class ReportersPool extends Reporter {

    /**
     * A list to hold one or more reporters. Add or
     * remove reporters to produce reports of different formats.
     */
    public List<StdReporter> reporters = new LinkedList<StdReporter>();

    public void setShowDepth(int d) {
        for (Reporter R : reporters) {
            R.setShowDepth(d);
        }
    }
    
    public void reportStep(ExecResult res, int stepNr) {
        for (Reporter R : reporters) {
            R.reportStep(res,stepNr) ;
        }
    }
    
    public void reportTraceBegin() {
        for (Reporter R : reporters) {
            R.reportTraceBegin();
        }
    }

    public void reportTraceEnd() {
        for (Reporter R : reporters) {
            R.reportTraceEnd();
        }
    }
    
    public void reportTestSetBegin(Class CUT, String header) {
        for (Reporter R : reporters) { R.reportTestSetBegin(CUT,header); }
    }
    
    public void reportTestSetEnd(Class CUT, TraceSetExecInfo info, String footer) {
        for (Reporter R : reporters) { R.reportTestSetEnd(CUT,info,footer); }
    }
    
    public void setOutStream(PrintStream ox) { 
        for (Reporter R : reporters) R.setOutStream(ox); 
    }
    
    static public final ReportersPool NULLreporter = new ReportersPool() ;    
   
}
