/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2;
//import Sequenic.T2.Seq.* ;

import Sequenic.T2.Msg.*;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * An instance of this class represents the content of a tr-file. Such a file is 
 * used to save traces for regression. To save a set of traces we first wrap them
 * as an TrFile object. Then we write the TrFile object to a file via serialization. 
 * So, indirectly, this class defines the format of a tr-file.
 */
public class TrFile implements Serializable {
	
	private static final long serialVersionUID = 1L;

    /**
     * Specify the name of the target class to which the traces belong to.
     */
    public String CUTname;
    /**
     * Specify the fully qualified name of the class of the pool 
     * used to produce the traces.
     */
    public String poolClassName;
    /**
     * Time stamp. Currently not used.
     */
    public String timestamp = null;
    /**
     * Serial number. Currently not used.
     */
    public String serialnr = null;
    /**
     * The name of the classes whose class invariants used during to test 
     * the traces.
     */
    public LinkedList<String> classinvariantOwners;
    /**
     * The traces to be saved.
     */
    public LinkedList<Sequenic.T2.Seq.Trace> traces;

    private TrFile(){ super() ; } ;
    
    /**
     * @param C The name of the target class to which the traces belong.
     * @param p The name of the class of the pool used to create the traces.
     * @param ts The traces.
     * @param invs The class invariants used in the production of ts.
     */
    public TrFile(String C, String p, LinkedList<Sequenic.T2.Seq.Trace> ts, List<Method> invs) {
        assert C != null && p != null && ts != null && invs != null : "PRE";
        CUTname = C;
        poolClassName = p;
        traces = ts;
        classinvariantOwners = new LinkedList<String>();
        for (Method cinv : invs) {
            classinvariantOwners.add(cinv.getDeclaringClass().getName());
        }
    }

    /**
     * For saving the content of this TrFile-object.
     */
    public void save(String fname) throws T2Exception {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fname));
            oos.writeObject(this);
            oos.close();
        } catch (NotSerializableException e) {
            throw new T2Error("A part of execution trace turns out to be unserializable.", e);
        } catch (InvalidClassException e) {
            throw new T2Error("A part of execution trace turns out to be unserializable.", e);
        } catch (Exception e) {
            throw new T2Exception("Fail to save execution traces.", e);
        }
    }

    public void save() throws T2Exception {
        save(CUTname + ".tr");
    }

    /**
     * For loading the content of a TrFile.
     */
    static public TrFile load(String fname) throws T2Exception {
        //System.out.println(">>>" + fname) ;
        TrFile tf = null;
        try {
            File file = new File(fname) ;
            //System.out.println(">>>" + file.exists()) ;
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fname));
            tf = (TrFile) ois.readObject();
            ois.close();
        } catch (Exception e) {
            //e.printStackTrace();
            throw new T2Exception("Fail to load execution traces.", e);
        }
        return tf;
    }

    /**
     * Deleting a TrFile.
     */
    static public void delete(Class C) {
        //System.out.println("*** " + C.getName() + ".tr") ;
        (new File(C.getName() + ".tr")).delete();
    }
    
    /**
     * Return a new TrFile that merges this TrFile and tf2. Both TrFile
     * should have the same CUTname, classinv-owners, and pool.
     * 
     * The method leaves the serialnr and timestamp fields null.
     */
    public TrFile merge(TrFile tf2) {
    	TrFile tfout = new TrFile() ;
    	tfout.CUTname = CUTname ;
    	tfout.classinvariantOwners = classinvariantOwners ;
    	tfout.poolClassName = poolClassName ;
    	tfout.traces = traces ;
    	tfout.traces.addAll(tf2.traces) ;
    	return tfout ;
    }
    
    
}
