/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Seq;

import Sequenic.T2.Pool;
//import Sequenic.T2.Obj.*;
import Sequenic.T2.Msg.*;
import Sequenic.P2.*;
import Sequenic.T2.Engines.Util ;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * A MkVal step where a collection like object is constructed. Set
 * collectionType to "ARRAY" to create an array, and else the name
 * of the class implementing Collection to make a collection.
 */
public class CREATE_COLLECTION_LIKE extends MkValStep {
	
	private static final long serialVersionUID = 1L;

    public String collectionType;
    public Class elementType;
    public int size;
    public MkValStep[] elements;

    public CREATE_COLLECTION_LIKE(String Ctype, Class ElemTy, int n, MkValStep[] elems) {
        collectionType = Ctype;
        elementType = ElemTy;
        size = n;
        elements = elems;
    }

    public Object exec(Class CUT, Pool pool) throws InvocationTargetException {
        
        Object result = null;
        try {
            if (collectionType.equals(MkValStep.ARRAY)) {
                result = Array.newInstance(elementType, size);
                for (int i = 0; i < size; i++) {
                    Array.set(result, i, elements[i].exec(CUT,pool));
                }
                return result;
            }
            // ELSE:

            result = (Collection) Class.forName(collectionType).newInstance();
            for (int i = 0; i < size; i++) {
                ((Collection) result).add(elements[i].exec(CUT,pool));
            }
        } catch (InvocationTargetException e) {
            throw e;
        } catch (Exception e) {
            throw new T2Error("Fail to create a collection " + this, e);
        }
        return result;
    }

    /**
     * For serialization.
     */
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        try {
            stream.writeObject(collectionType);
            stream.writeObject(elementType.getName());
            stream.writeInt(size);
            stream.writeObject(elements);
        } catch (Exception e) {
            throw new IOException();
        }
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        try {
            collectionType = (String) stream.readObject();
            String elementType_ = (String) stream.readObject();
            elementType = Util.classOf(elementType_);
            size = stream.readInt();
            elements = (MkValStep[]) stream.readObject();
        } catch (ClassNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new IOException();
        }
    }

    @Override
    public String toString() {
        String result = "CREATE_COLLECTION_LIKE of type " + collectionType + ": ";

        String elemsTxt = "";
        for (int i = 0; i < elements.length; i++) {
            elemsTxt = elemsTxt + elements[i];
            if (i < elements.length - 1) {
                elemsTxt = elemsTxt + ",\n";
            }
        }
        result = result + "[ " + StringFormater.indentButFirst(elemsTxt, 4) + " ]";

        return result;
    }
}
