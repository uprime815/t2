package Sequenic.T2.Engines;

import java.lang.reflect.* ;
import java.util.* ;
import org.junit.Test;

/**
 *
 * @author wishnu
 */
public class UtilTest {

    static public class C_isMainMethod {
        public void m(String[] args) { }
        static public void main(String arg) { }
        static public void main(String[] args) { }
    }
    
    @Test
    public void test_isMainMethod() {
        Method META_m = null ;
        Method META_main1 = null ;
        Method META_main2 = null ;
        try {
            String[] a_string_array = new String[0] ;
            Class string_ar_ty = a_string_array.getClass() ;
            META_m = C_isMainMethod.class.getDeclaredMethod("m", new Class[] { string_ar_ty } ) ;
            META_main1 = C_isMainMethod.class.getDeclaredMethod("main", new Class[]{ String.class }) ;            
            META_main2 = C_isMainMethod.class.getDeclaredMethod("main", new Class[]{ string_ar_ty }) ;
    
        }
        catch(Exception e) { assert false ; }
        assert ! Util.isMainMethod(META_m) ;
        assert ! Util.isMainMethod(META_main1) ;
        assert Util.isMainMethod(META_main2) ;
    }
    
    
    static class C_field0 {
        private int x0 ;  private int x0b ;
        int x1 ; int x1b ;
        protected int x2 ; protected int x2b ;
        public int x3 ; public int x3b ;
        static public int xs0 ;  static public int xs0b ;
    }
    
    static class C_field1 extends C_field0 {
        private int x0 ;
        int x1 ;
        protected int x2 ;
        public int x3 ;
        static public int xs0 ;
    }
    
    static public class C {
        public static void bla() { }
    }
    
    static public class C1 extends C {
        
    }
    
    @Test 
    public void test_static_method_inheritence() {
        C1.bla();
    }
    
    @Test
    public void test_getAllField() {
        try {
            Class C = C_field1.class ;
            Class D = C_field0.class ;
            Field g_x0 = D.getDeclaredField("x0") ;
            Field g_x1 = D.getDeclaredField("x1") ;
            Field g_x2 = D.getDeclaredField("x2") ;
            Field g_x3 = D.getDeclaredField("x3") ;
            Field g_x0b = D.getDeclaredField("x0b") ;
            Field g_x1b = D.getDeclaredField("x1b") ;
            Field g_x2b = D.getDeclaredField("x2b") ;
            Field g_x3b = D.getDeclaredField("x3b") ;
            Field g_xs0 = D.getDeclaredField("xs0") ;
            Field g_xs0b = D.getDeclaredField("xs0b") ;
            
            Field f_x0 = C.getDeclaredField("x0") ;
            Field f_x1 = C.getDeclaredField("x1") ;
            Field f_x2 = C.getDeclaredField("x2") ;
            Field f_x3 = C.getDeclaredField("x3") ;
            Field f_xs0 = C.getDeclaredField("xs0") ;
            
            List<Field> fs0 = Util.getAllFields(D) ;
            assert fs0.contains(g_x0) ;
            assert fs0.contains(g_x1) ;
            assert fs0.contains(g_x2) ;
            assert fs0.contains(g_x3) ;
            assert ! fs0.contains(g_xs0) ;
            assert fs0.size()==8 ;
            
            List<Field> fs1 = Util.getAllFields(C) ;
            assert fs1.contains(f_x0) ;
            assert fs1.contains(f_x1) ;
            assert fs1.contains(f_x2) ;
            assert fs1.contains(f_x3) ;
            assert ! fs1.contains(g_x0) ;
            assert ! fs1.contains(g_x1) ;
            assert ! fs1.contains(g_x2) ;
            assert ! fs1.contains(g_x3) ;            
            assert ! fs1.contains(g_x0b) ;
            assert fs1.contains(g_x1b) ;
            assert fs1.contains(g_x2b) ;
            assert fs1.contains(g_x3b) ;                 
            assert fs1.size()==7 ;
        }
        catch(Exception e) { assert false ; }

    }
    
    static class C_method0 {
        private void x0() { } ;  
        private void x0b() { } ;
        void x1() { } ; 
        void x1b() { } ;
        protected void x2() { } ; 
        protected void x2b() { } ;
        public void x3() { } ; 
        public void x3b() { } ;
        static public void xs0() { } ;  
        static public void xs0b() { } ;
    }
    
    static class C_method1 extends C_method0 {
        private void x0() { } ;
        void x1() { } ;
        protected void x2() { } ;
        public void x3() { } ;
        static public void xs0() { } ;
    }
    
    
    @Test
    public void test_getAllMeths() {
        Class C = C_method1.class ;
        Class D = C_method0.class ;
        Class[] paramsTy = new Class[0] ;
        try {
            Method n_x0 = D.getDeclaredMethod("x0",  paramsTy) ;
            Method n_x1 = D.getDeclaredMethod("x1", paramsTy) ;
            Method n_x2 = D.getDeclaredMethod("x2", paramsTy) ;
            Method n_x3 = D.getDeclaredMethod("x3", paramsTy) ;
            Method n_x0b = D.getDeclaredMethod("x0b", paramsTy) ;
            Method n_x1b = D.getDeclaredMethod("x1b", paramsTy) ;
            Method n_x2b = D.getDeclaredMethod("x2b", paramsTy) ;
            Method n_x3b = D.getDeclaredMethod("x3b", paramsTy) ;
            Method n_xs0 = D.getDeclaredMethod("xs0", paramsTy) ;
            Method n_xs0b = D.getDeclaredMethod("xs0b", paramsTy) ;
            
            Method m_x0 = C.getDeclaredMethod("x0", paramsTy) ;
            Method m_x1 = C.getDeclaredMethod("x1", paramsTy) ;
            Method m_x2 = C.getDeclaredMethod("x2", paramsTy) ;
            Method m_x3 = C.getDeclaredMethod("x3", paramsTy) ;
            Method m_xs0 = C.getDeclaredMethod("xs0", paramsTy) ;
            
            List<Method> ms0 = Util.getAllMeths(D) ;
            assert ms0.contains(n_x0) ;
            assert ms0.contains(n_x1) ;
            assert ms0.contains(n_x2) ;
            assert ms0.contains(n_x3) ;
            assert ms0.contains(n_xs0) ;
            assert ms0.size()==10 ;
            
            List<Method> ms1 = Util.getAllMeths(C) ;
            assert ms1.contains(m_x0) ;
            assert ms1.contains(m_x1) ;
            assert ms1.contains(m_x2) ;
            assert ms1.contains(m_x3) ;
            assert ms1.contains(m_xs0) ;
            assert ! ms1.contains(n_x0) ;
            assert ! ms1.contains(n_x1) ;
            assert ! ms1.contains(n_x2) ;
            assert ! ms1.contains(n_x3) ; 
            assert ! ms1.contains(n_xs0) ;
            assert ! ms1.contains(n_x0b) ;
            assert ms1.contains(n_x1b) ;
            assert ms1.contains(n_x2b) ;
            assert ms1.contains(n_x3b) ; 
            assert ms1.contains(n_xs0b) ;
            assert ms1.size()==9 ;
        }
        catch (Exception e){ }
    
    }
    
    
}