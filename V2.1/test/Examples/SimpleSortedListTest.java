package Examples;
import Sequenic.T2.* ;
import org.junit.Test;
import static org.junit.Assert.*;

public class SimpleSortedListTest {
    
    @Test
    public void test() {
        // Measured coverage: practically 100%
        Main.Junit(SimpleSortedList.class.getName() + " --violmax=3 --savegoodtr=3") ;
    }

}