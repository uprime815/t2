package Sequenic.T2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    Sequenic.T2.MainTest.class,
    Sequenic.T2.Main_Test_1.class,
    Sequenic.T2.Main_Test_2.class,
    Sequenic.T2.Main_Test_3.class,
    Sequenic.T2.Main_Test_4.class,
    Sequenic.T2.Main_Test_5.class,
    Sequenic.T2.Main_Test_6.class,
    Sequenic.T2.Main_Test_7.class
    })
public class MainTestSuite {

}