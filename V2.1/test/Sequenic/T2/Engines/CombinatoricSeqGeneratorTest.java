package Sequenic.T2.Engines;

import java.util.* ;
import org.junit.Test;

public class CombinatoricSeqGeneratorTest {

    static public class C {        
        public int x ;
        public void m1() { }
        public void m2() { }
    }
    

    static void printSequence(List seq) {
        System.out.print(">>> ") ;
        for (int i=0; i<seq.size();i++) 
            System.out.print("; " + seq.get(i)) ;
        System.out.println("") ;
    }
    
    @Test
    public void test0() {
        System.out.println(">>> testing next_sequence ...") ;
        BaseEngine engine = new BaseEngine(C.class) ;
        CombinatoricSeqGenerator seqGen = new CombinatoricSeqGenerator(engine) ;
        seqGen.r = 3 ;
        seqGen.duplicates = 1 ;
        seqGen.init() ;
        int N = seqGen.totalNumberOfCombinations() ;
        System.out.println("#comb(3,3) with dup=1 ... is " + N) ;
        assert N == 6 ;
        List seq ;
        for (int i=0; i<N; i++) {
            seq = seqGen.next_sequence() ;
            printSequence(seq) ;
        }
        assert seqGen.next_sequence() == null ;
    }
    
    @Test
    public void test1() {
        System.out.println(">>> testing next_sequence ... with multiple replicas ") ;
        BaseEngine engine = new BaseEngine(C.class) ;
        CombinatoricSeqGenerator seqGen = new CombinatoricSeqGenerator(engine) ;
        seqGen.r = 2 ;
        seqGen.duplicates = 2 ;
        seqGen.init() ;
        int N = seqGen.totalNumberOfCombinations() ;
        System.out.println("#comb(3,2) with dup=2 ... is " + N) ;
        assert N == 12 ;
        List seq ;
        for (int i=0; i<N; i++) {
            seq = seqGen.next_sequence() ;
            printSequence(seq) ;
        }
        assert seqGen.next_sequence() == null ;
    }
    
    static public class C2 {        
        public int x ;
        public C2() { } 
        public C2(int y){ }
        public void m1() { }
    }
    
    @Test
    public void test2() {
        System.out.println(">>> testing next_sequence ... with multiple constructors") ;
        BaseEngine engine = new BaseEngine(C2.class) ;
        CombinatoricSeqGenerator seqGen = new CombinatoricSeqGenerator(engine) ;
        seqGen.r = 2 ;
        seqGen.duplicates = 1 ;
        seqGen.init() ;
        int N = seqGen.totalNumberOfCombinations() ;
        System.out.println("#comb(2,2) with dup=1 ... is " + N) ;
        assert N == 4 ;
        List seq ;
        for (int i=0; i<N; i++) {
            seq = seqGen.next_sequence() ;
            printSequence(seq) ;
        }
        assert seqGen.next_sequence() == null ;
    }

}