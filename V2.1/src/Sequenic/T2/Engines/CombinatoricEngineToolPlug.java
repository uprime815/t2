package Sequenic.T2.Engines;import jargs.gnu.*;

public class CombinatoricEngineToolPlug extends BaseEngineToolPlug {
    
    private CmdLineParser.Option numOfDuplicatesO = parser.addIntegerOption("dup");
    
    public CombinatoricEngineToolPlug() {
        super() ;
        addOption(numOfDuplicatesO, "Number of duplicates per sequence.");
    }
    
    @Override
    public void configure(String[] options) 
                throws 
                CmdLineParser.IllegalOptionValueException, 
                CmdLineParser.UnknownOptionException {
         
        super.configure(options);
        
        // Plug a different sequence generator:
        CombinatoricSeqGenerator seqGen = new CombinatoricSeqGenerator(engine) ;
        seqGen.r = engine.maxExecLength-1 ;
        seqGen.duplicates = (Integer) parser.getOptionValue(numOfDuplicatesO,seqGen.duplicates) ; 
        seqGen.init() ;
        engine.seqGenerator = seqGen ;
        config.add("Number of duplicates per sequence = " + seqGen.duplicates) ;
                 
     }
    

}
