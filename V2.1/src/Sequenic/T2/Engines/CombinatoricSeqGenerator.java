/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Engines;

import java.util.* ;
import java.lang.reflect.* ;


/**
 *
 * @author wishnu
 */
public class CombinatoricSeqGenerator extends AbstractCombinatoricSeqGenerator {

    public CombinatoricSeqGenerator(BaseEngine e) { 
        engine = e ; 
    }
    
    public int r = 3 ;
    public int duplicates = 2 ;
    protected Constructor[] cons_scope ;
    protected Object[] fields_and_methods_scope ;
    
    AbstractCombinationGenerator combGenerator ;
    
    public void init() {
        int i ;
        cons_scope = new Constructor[engine.consIPs.size()] ;
        i = 0 ;
        for (Constructor con : engine.consIPs) {
            cons_scope[i] = con ;
            i++ ;
        }
        List<Method> methods = engine.getMethodsCanAcceptCUTinRec() ;
        Map<Method,Method> specmap = engine.getMethodSpecs() ;
        
        fields_and_methods_scope = new Object[engine.fieldsIPs.size()
                                        + methods.size()] ;
        
        assert cons_scope.length > 0 && fields_and_methods_scope.length > 0 ;
        
        i=0 ;
        for (Method m : methods) {
            if (specmap.containsKey(m)) fields_and_methods_scope[i] = specmap.get(m) ;
            else fields_and_methods_scope[i] = m ;
            i++ ;
        } 
            
        for (Field f : engine.fieldsIPs) {
            fields_and_methods_scope[i] = f ;
            i++ ;
        }

        combGenerator = new OrderedCombinationGenerator(fields_and_methods_scope.length,r) ;
        // Innitializing counters:
        current_cons =  cons_scope.length ;
        current_dup = duplicates ;
    }
          
    private int current_cons ;
    private int current_dup ;
    private List current_seq = new LinkedList() ;
    
    private void get_new_mseq() {
        // Clear current sequence, and add a dummy first element
        current_seq.clear() ;
        current_seq.add(null);
        // Get a new combination and fill in the steps:
        int[] indices = combGenerator.getNext() ;
        for (int i=0; i<indices.length; i++) current_seq.add(fields_and_methods_scope[indices[i]]) ;
    }
    
    private boolean done() {
        return (current_cons+1 >= cons_scope.length)
                && (current_dup >= duplicates)
                && !combGenerator.hasMore() 
                ;
    }
    
    private boolean nextCombination() {
        return (current_cons+1 >= cons_scope.length)
                && (current_dup >= duplicates)
                && combGenerator.hasMore() 
                ;
    }
    
    private boolean nextConstructor() {
        return (current_cons+1 < cons_scope.length)
                && (current_dup >= duplicates)
                ;
    }
    
    private boolean nextDuplicate() {
        return current_dup < duplicates ;
    }
    
    
    @Override
    public List next_sequence() {
        
        if (done()) return null ;
        
        if (nextDuplicate()) {
            current_dup++ ;
            return current_seq ;
        }
        
        if (nextConstructor()) {
            current_cons++ ;
            current_dup=1 ;
            current_seq.set(0,cons_scope[current_cons]) ;
            return current_seq ;
        }
        
        if (nextCombination()) {
            current_cons = 0 ;
            current_dup = 1 ;
            get_new_mseq() ;
            current_seq.set(0,cons_scope[current_cons]) ;
            return current_seq ;
        }
        assert false ;
        return null ;
    }
    
    public int totalNumberOfCombinations() {
        return  (combGenerator.getTotal()).intValue() * duplicates * cons_scope.length ;
    }

}
