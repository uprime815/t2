
package Sequenic.T2.DataGen;
import java.security.SecureRandom;
import org.uncommons.maths.random.*;

/**
 * Adapted from {@link org.uncommons.maths.random.SecureRandomSeedGenerator}.
 * 
 * <p>This is a seed generator based on Java's {@link SecureRandom} to generate
 * the seed.
 */
public class T2StandardSeedGenerator implements SeedGenerator
{
    private static final SecureRandom SOURCE = new SecureRandom();

    public byte[] generateSeed(int length) throws SeedException
    {
        byte[] seed = SOURCE.generateSeed(length);
        //System.out.println(length + " bytes of seed data acquired from java.security.SecureRandom");
        return seed;
    }
}