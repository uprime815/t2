package Sequenic.T2.DataGen;
import java.util.* ;
import org.uncommons.maths.*;
import org.uncommons.maths.random.*;

/**
 * This generates a number between 0..1 with a uniform distribution. If
 * it is less than high, then it will be interpreted as "true". If high
 * is 0 or null then it will always return false.
 */
public class RandomBoolUniform extends RandomBool {

    private NumberGenerator<Double> generator;

    public RandomBoolUniform(double treshold) {
        low = null;
        high = treshold;
        try {
            generator = new ContinuousUniformGenerator(0, 1, new Random()) ;
            // Turning off MersenneTwister due to issue with linux
            //generator = new ContinuousUniformGenerator(0, 1, new MersenneTwisterRNG(new SecureRandomSeedGenerator()));
        } catch (Exception e) {
            assert false ;
            // generator = new ContinuousUniformGenerator(0, 1, new MersenneTwisterRNG());
        }
    }

    public boolean nextValue() {
        if (high == null || high == 0) {
            return false;
        } else {
            return generator.nextValue() < high;
        }
    }
}
