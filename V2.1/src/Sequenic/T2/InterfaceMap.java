package Sequenic.T2 ;


/**
 * A class representing interface map --see the explanation in the
 * package {@link Sequenic.T2 Sequenic.T2}. This class is abstrat; the class 
 * {@link Sequenic.T2.InterfaceMap0 InterfaceMap0} provides a predefined implementation.
 *
 * @see Sequenic.T2.RndEngine#RndTest
 */
abstract public class InterfaceMap {

	/**
	 * Obtain an implementation of an interface I. Return null if it
	 * can't find I or an implementation of it in the interface map.
	 */
	abstract public Class getImplementation(Class I) ;
    
}