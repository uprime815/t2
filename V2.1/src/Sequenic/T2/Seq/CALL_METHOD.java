/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;

import Sequenic.T2.Obj.*;
import Sequenic.T2.Pool;
import Sequenic.T2.Msg.*;
import Sequenic.P2.* ;
import java.lang.reflect.*;
import java.io.*;
import Sequenic.T2.Engines.Util ;
import java.util.List;

/**
 * This represents a test-step where we call a method.
 */
public class CALL_METHOD extends TraceStep {

	private static final long serialVersionUID = 1L;
	
	public Method method;
    // When it is null then the method is static. Else it is
    // the receiver of the call.
    public MkValStep receiver;
    public MkValStep[] params;

    /**
     * @param m The method to call. If the method is static, set r to null.
     * @param r MkVal step producing the receiver object.
     * @param ps MkVal steps producing the arguments for r.
     */
    public CALL_METHOD(Method m, MkValStep r, MkValStep[] ps) {
        method = m;
        receiver = r;
        params = ps;
    }

    public String getName() {
        return method.getName();
    }

    /**
     * For serialization.
     */
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
    	//System.out.println("DEBUG: writeObject 1");
        stream.writeObject(method.getDeclaringClass().getName());
        
        //System.out.println("DEBUG: writeObject 2");
        stream.writeObject(method.getName());
        
        Class[] paramTypes = method.getParameterTypes();
        String[] paramTypes_ = new String[paramTypes.length];
        for (int i = 0; i < paramTypes.length; i++) {
            paramTypes_[i] = paramTypes[i].getName();
        }
        //System.out.println("DEBUG: writeObject 3");
        stream.writeObject(paramTypes_);
        
        //System.out.println("DEBUG: writeObject 4");
        stream.writeObject(receiver);
        
        //System.out.println("DEBUG: writeObject 5");
        stream.writeObject(params);
        
        //System.out.println("DEBUG: writeObject 6");
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        try {
            Class C = Util.classOf((String) stream.readObject());
            String methodName = (String) stream.readObject();
            String[] paramTypes_ = (String[]) stream.readObject();
            Class[] paramTypes = new Class[paramTypes_.length];
            for (int i = 0; i < paramTypes_.length; i++) {
                paramTypes[i] = Util.classOf(paramTypes_[i]);
            }
            // method = C.getMethod(methodName,paramTypes) ;
            method = Util.getMethod(C, methodName, paramTypes);
            receiver = (MkValStep) stream.readObject();
            params = (MkValStep[]) stream.readObject();
        } catch (ClassNotFoundException e) {
            throw e;
        } catch (Exception e) {
            // System.out.println("##" + e) ;
            throw new IOException();
        }
    }

    public ExecResult exec(Class CUT,
            Pool pool, 
            Object targetObj, 
            int stepNr,
            List<Method> classinvs, 
            ReportersPool reporters) {
        
        method.setAccessible(true);
        
        SlotsInjector.injectAtTestStep(targetObj, this);
        
        ExecResult result = new ExecResult(CUT,this,targetObj);
        
        // Construct the arguments first:
        try {     
            result.args = MkValStep.MkArguments(CUT, pool, params); 
            // If receiver null, then it is a static method, which has no receiver,
            // else construct the receiver:
            if (receiver != null) result.receiverObj = receiver.exec(CUT,pool);
        }
        catch (InvocationTargetException e) {
            result.argumentFailure = e.getCause() ;
            result.checkException(e) ;
        }
        catch (Exception e) {
            // System.out.println(Show.show(receiver)) ;
            // e.printStackTrace() ;
            throw new T2Error(T2Error.BUG, "Fail to invoke method " + this, e);
        }
       
        if (result.argumentFailure == null) {
            // If arguments are sucessfully constructed, call the method:            
            Class D = method.getDeclaringClass();
            // also casting receiver to D:
            try {
                result.returnedObj = method.invoke(D.cast(result.receiverObj), result.args);
                result.execClassInv(classinvs, targetObj);
            }                    
            catch (InvocationTargetException e) {
                result.checkException(e) ;
            }
            catch (Exception e) {
                throw new T2Error(T2Error.BUG, "Fail to invoke method " + this, e);
            }
        }

        // Report, unless we have a NULLreporter:    
        reporters.reportStep(result, stepNr) ;
       
        return result;
    }
    
    @Override
    public String toString() {
        String result = "CALL_METHOD, " + method.getName() ;
        String paramsTxt = "" ;
        for (int i=0; i<params.length; i++) {
            paramsTxt = paramsTxt + params[i] ;
            if (i<params.length-1) paramsTxt = paramsTxt + ",\n" ;
        }
        result = result + " (" 
                + StringFormater.indentButFirst(paramsTxt,4)
                +")" ;
        if (receiver != null) result = result + " ON " + receiver ;
        
        return result ;
        
    }

}
